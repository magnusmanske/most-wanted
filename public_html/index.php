<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( "php/common.php") ;

$dir = '/data/project/most-wanted/data' ;

$script = $_SERVER['PHP_SELF'] ;

function get_categories_of_page ( $db , $page , $ns = 0 ) {
#	global $slow_ok_limit ;
#	$mysql_con = db_get_con_new($language,$project) ;
#	$db = get_db_name ( $language , $project ) ;
	make_db_safe ( $page ) ;
	
	$ret = array () ;
	$sql = "SELECT cl_to,cl_sortkey,cl_timestamp FROM page,categorylinks WHERE page_title=\"$page\" AND page_namespace=$ns AND cl_from=page_id" ;
#	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
#	if ( mysql_errno() != 0 ) return $ret ; // Something's broken
#	while ( $o = mysql_fetch_object ( $res ) ) {
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret[$o->cl_to] = $o ;
	}
	return $ret ;
}

function db_get_existing_pages ( $db , $titles , $namespace = 0 , $remove_redirects = 0 ) {
#	$mysql_con = db_get_con_new($language,$project) ;
#	$db = $language . 'wiki_p' ;
	foreach ( $titles AS $k => $v ) make_db_safe ( $titles[$k] ) ;
	$ret = array () ;
	
	while ( count ( $titles ) > 0 ) {
		$t2 = array () ;
		while ( count ( $t2 ) < 100 && count ( $titles ) > 0 ) $t2[] = array_pop ( $titles ) ;
		$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=$namespace AND page_title IN (\"" . implode ( "\",\"" , $t2 ) . "\")" ;
		if ( $remove_redirects ) $sql .= " AND page_is_redirect=0" ;
#		$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
#		if ( mysql_errno() != 0 ) return $ret ;#{ print 'Line 171:' . mysql_error() . "<br/>" ; return $ret ; } # Some error has occurred
#		while ( $o = mysql_fetch_object ( $res ) ) {
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$ret[$o->page_title] = $o->page_title ;
		}
	}
	return $ret ;	
}

function db_get_incoming_page_links ( $db , $title , $ns_page = 0 , $ns_link = 0 ) {
#	$mysql_con = db_get_con_new($language,$project) ;
#	$db = $language . 'wiki_p' ;
	make_db_safe ( $title ) ;
	
	$ret = array () ;
	$sql = "SELECT * FROM pagelinks,page WHERE pl_from=page_id AND pl_title=\"{$title}\" AND page_namespace=$ns_page AND pl_namespace=$ns_link" ;
#	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
#	if ( mysql_errno() != 0 ) return $ret ;# Some error has occurred
#	while ( $o = mysql_fetch_object ( $res ) ) {
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		if ( $o->page_title == '' ) continue ;
		$ret[$o->page_title] = $o->page_title ;
	}
	return $ret ;	
}

function db_get_template_usage ( $db , $tltitles , $ns = 0 , $tlns = 10 ) {
	$ret = array () ;
	if ( 0 == count ( $tltitles ) ) return $ret ;
	
#	$mysql_con = db_get_con_new($language,$project) ;
#	$db = get_db_name ( $language , $project ) ;
	$titles = array () ;
	foreach ( $tltitles AS $t ) {
		make_db_safe ( $t ) ;
		$titles[$t] = '"' . $t . '"' ;
	}
	$titles = implode ( ',' , $titles ) ;
	
	
	$sql = "SELECT DISTINCT page_title FROM page,templatelinks,linktarget WHERE tl_target_id=lt_id AND lt_namespace=$tlns AND page_namespace=$ns AND lt_title IN ($titles) AND page_id=tl_from" ;
//	print "$sql<br/>" ;
#	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
#	while ( $o = mysql_fetch_object ( $res ) ) {
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret[$o->page_title] = $o->page_title ;
	}
	return $ret ;
}

function linked_via_templates ( $article , $language , $project ) {
	global $db ;
	$lft = db_get_incoming_page_links ( $db , $article , 10 , 0 ) ; // Links from templates
	$tl = db_get_template_usage ( $db , $lft ) ;
	return count($tl) ;
}

print get_common_header ( '' , 'Most wanted' ) ;
/*
print "<html><body>" ;
print '<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="lib/sorttable.js"></script>
<style type="text/css">
table.sortable thead {
    background-color:#eee;
    font-weight: bold;
    cursor: default;
}
</style>
</head>' ;
print get_common_header ( "met.php" ) ;
print "<body><h1>Most wanted articles</h1>" ;
*/

$project = get_request ( 'project' , 'enwiki' ) ;
$start = get_request ( 'start' , 1 ) ;
$number = get_request ( 'number' , 50 ) ;
$hints = get_request ( 'hints' , 0 ) ;

$fullpath = '' ;
$is_valid_project = false ;
$links = array () ;
$dh  = opendir ( $dir ) ;
while (false !== ($filename = readdir($dh))) {
	$a = array () ;
	if ( !preg_match ( '/^(.+)_p\.links\.red$/' , $filename , $a ) ) continue ;
	$prj = $a[1] ;
	if ( $prj == $project ) {
		$links[$prj] = "<b>$prj</b>" ;
		$is_valid_project = true ;
		$fullpath = "$dir/$filename" ;
	} else $links[$prj] = "<a href='$script?hints=$hints&project=$prj&number=$number'>$prj</a>" ;
}
closedir ( $dh ) ;

print "<table class='table'>" ;
print "<tr><td>Available projects</td><td>" . implode ( " | " , $links ) . "</td></tr>" ;

if ( !$is_valid_project ) {
	print "</table>" ;
	print "Not a valid project : \"$project\"" ;
	exit ( 0 ) ;
}

$entries = count( file( $fullpath));
$ft = filemtime ( $fullpath ) ;

$links = array() ;
foreach ( array ( 20 , 50 , 100 , 250 , 500 , 1000 ) AS $k ) {
	if ( $number == $k ) $links[] =  "<b>$k</b>" ;
	else $links[] =  "<a href='$script?hints=$hints&project=$project&number=$k'>$k</a>" ;
}
print "<tr><td>Number of articles</td><td>" . implode ( " | " , $links ) . "</td></tr>" ;
print "<tr><td>Navigation</td><td>" ;

$end = $entries - $number + 1 ;
$a = array() ;
if ( $start > 1 ) {
	$a[] = "<a href='$script?hints=$hints&project=$project&number=$number'>Start</a>" ;
	$a[] = "<a href='$script?hints=$hints&project=$project&number=$number&start=" . ( $start - $number ) . "'>Previous $number</a>" ;
} else {
	$a[] = "Start" ;
	$a[] = "Previous $number" ;
}
if ( $start < $end ) {
	$a[] = "<a href='$script?hints=$hints&project=$project&number=$number&start=" . ( $start + $number ) . "'>Next $number</a>" ;
	$a[] = "<a href='$script?hints=$hints&project=$project&number=$number&start=$end'>End</a>" ;
} else {
	$a[] = "Next $number" ;
	$a[] = "End" ;
}

$r = mt_rand ( 0 , $entries==0?0:$entries - 1 ) ;
$r = floor ( $r / $number ) ;
$r = $r * $number + 1 ;
$a[] = "<a href='$script?hints=$hints&project=$project&number=$number&start=$r'>Random</a>" ;

$navlinks = join ( ' | ' , $a ) ;
print $navlinks ;
print "</td></tr>" ;
print "<tr><td>Hints</td><td>" ;
if ( $hints ) print "<a href='$script?hints=0&project=$project&number=$number&start=$start'>Hide hints</a> (faster)" ;
else print "<a href='$script?hints=1&project=$project&number=$number&start=$start'>Show hints</a> (slower)" ;
print "</td></tr>" ;
print "</table>" ;

$baseurl = '' ;
$language1 = '' ;
$project1 = '' ;
$a = array () ;
if ( preg_match ( '/^(.+)wiki$/' , $project , $a ) ) {
	$baseurl = "http://{$a[1]}.wikipedia.org" ;
	$language1 = $a[1] ;
	$project1 = 'wikipedia' ;
} else if ( preg_match ( '/^(.+)wikisource$/' , $project , $a ) ) {
	$baseurl = "http://{$a[1]}.wikisource.org" ;
	$language1 = $a[1] ;
	$project1 = 'wikisource' ;
} else {
	print "Cannot compute base URL for $project" ;
	exit ( 0 ) ;
}

$db = openDB ( $language1 , $project1 ) ;

print "<p>" ;
print "List was last updated " . date ( "F d, Y H:i:s" , $ft ) . " (UTC). " ;
print "There are $entries potential articles with at least 10 incoming links from article namespace." ;
print "</p>" ;


$f = fopen ( $fullpath , "r" ) ;
for ( $a = 1 ; $a < $start ; $a++ ) fgets ( $f ) ;

$articles = array () ;
$articles_ucf = array () ;
for ( $a = 0 ; $a < $number ; $a++ ) {
	$s = fgets ( $f ) ;
	$articles[] = $s ;
	$articles_ucf[] = ucfirst ( strtolower ( $s ) ) ;
}
$ex = db_get_existing_pages ( $db , $articles , 0 , 0 ) ;
$ex2 = db_get_existing_pages ( $db , $articles_ucf , 0 , 0 ) ;


//if ( isset ( $_REQUEST['test'] ) ) { print "<pre>$language1/$project1\n" ; print_r ( $ex ) ; print "</pre>" ; }

print "<table class='table table-condensed table-striped sortable'><thead><tr><th>Rank</th><th>Article</th><th>Incoming article links</th><th>Search the web</th><th>Non-template links (est.)</th>" ;
if ( $hints ) print "<th>Hints (categories of linking articles)</th>" ;
print "</thead></tr>" ;
$cur = 0 ;
foreach ( $articles AS $s ) {
	$parts = array () ;
	preg_match ( '/^\s*(\d+)\s(.+)$/' , $s , $parts ) ;
	$count = $parts[1] ?? 0;
	$article = $parts[2] ?? '';
	$pretty = str_replace ( '_' , ' ' , $article ) ;
	$num = $cur + $start ;
	$article2 = str_replace ( "'" , urlencode("'") , $article ) ;
	$style = "style='color:red'" ;
	if ( isset ( $ex[$article] ) ) $style = '' ;
	
	$gl = "http://www.google.co.uk/search?ie=UTF-8&q=" . urlencode ( $pretty ) ;
	$gwpl = "http://www.google.co.uk/search?ie=UTF-8&q=" . urlencode ( "$pretty site:wikipedia.org" ) ;
	
	print "<tr><th>#$num</th>" ;
	print "<td><a $style href='$baseurl/wiki/$article2'>$pretty</a></td>" ;
	print "<td><a href='$baseurl/w/index.php?title=Special%3AWhatLinksHere&target=$article2&namespace=0'>$count</a></td>" ;
	print "<td><a href='$gl'>G</a> | <a href='$gwpl'>GWP</a></td>" ;
	
	$vp = linked_via_templates ( $article , $language1 , $project1 ) ;
	$ln = $count - $vp ;
	if ( $ln < 0 ) $ln = 0 ;
	if ( $ln >= 10 ) print "<td><b>$ln</b></td>" ;
	else print "<td>$ln</td>" ;
	
	if ( $hints ) {
		$lf = db_get_incoming_page_links ( $db , $article , 0 , 0 ) ; // Links from pages
		$lfc = array () ;
		foreach ( $lf AS $l ) {
			$lc = get_categories_of_page ( $db , $l , 0 ) ;
			foreach ( $lc AS $c => $d ) {
				$c = str_replace ( '_' , ' ' , $c ) ;
				if ( !isset ( $lfc[$c] ) ) $lfc[$c] = 0 ;
				$lfc[$c]++ ;
			}
			$top = max ( $lfc ) ;
			if ( $top >= 10 ) break ;
		}
		foreach ( $lfc AS $c => $v ) {
			if ( $v < $top ) unset ( $lfc[$c] ) ;
			else if ( preg_match ( '/\barticles\b/i' , $c ) ) unset ( $lfc[$c] ) ;
			else if ( preg_match ( '/\bliving people\b/i' , $c ) ) unset ( $lfc[$c] ) ;
			else if ( $c == "Mann" || $c == "Frau" ) unset ( $lfc[$c] ) ;
		}
		print "<td style='font-size:80%'>" . implode ( " | " , array_keys ( $lfc ) ) . "</td>" ;
	}
	
	$aucf = ucfirst ( strtolower ( $article ) ) ;
	if ( isset ( $ex2[$aucf] ) ) {
		$aucf2 = str_replace ( "'" , urlencode("'") , $aucf ) ;
		print "<td>See <a href='$baseurl/wiki/$aucf2'>$aucf</a></td>" ;
	}
	print "</tr>" ;
	$cur++ ;
}
fclose ( $f ) ;
print "</table>" ;

print "<p>$navlinks</p>" ;

print get_common_footer() ;

# Logging
require_once ( "php/ToolforgeCommon.php" ) ;
$tfc = new ToolforgeCommon('most-wanted') ;
$tfc->logToolUse('',$project) ;

?>
