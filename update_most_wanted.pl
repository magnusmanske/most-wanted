#!/usr/bin/perl

use strict ;
use warnings ;
use DBI ;

my ( $wiki ) = @ARGV ; # e.g. enwiki_p

my $user ;
my $db_password ;
open FILE , '/data/project/most-wanted/replica.my.cnf' ;
while ( <FILE> ) {
	if ( $_ =~ m/^user\s*=\s*'(.+)'\s*$/ ) { $user = $1 ; }
	if ( $_ =~ m/^password\s*=\s*'(.+)'\s*$/ ) { $db_password = $1 ; }
}
close FILE ;

my %ignore ; # Patterns for articles to ignore
$ignore{'dewiki_p'} = [ qr/Droga_wojewódzka_\d+/ , qr/Europastraße_\d+/ , qr/\bDivision\b/ ] ; #, qr/^\.[a-z]+$/ , qr/^[A-Z]+_\d+$/ , qr/^[A-Z_]+[0-9][A-Z0-9_]+$/ ] ;
$ignore{'enwiki_p'} = [ qr/\bDivision\b/ ] ; #, qr/^[A-Z_]+_\d+$/ , qr/^[A-Z_]+[0-9][A-Z0-9_]+$/ ] ;

my $minreq = 10 ;
my $query_group = 20 ;
my @articles ;
my $dbh ;

my $outfile = "/data/project/most-wanted/data/$wiki.links.red" ;

$wiki =~ m/^(.+)wiki_p$/ ;
my $l = $1 ;
my $p = 'wiki' ;
my $server = "$l$p.labsdb" ;

$dbh = DBI->connect( "DBI:mysql:$wiki;host=$server",$user,$db_password) || die "Database connection not made: $DBI::errstr";

# Write tmp file
open OUT , ">$outfile.tmp1" ;
my $sql = "SELECT count(*) AS cnt,lt_title FROM pagelinks,linktarget WHERE pl_target_id=lt_id AND lt_namespace=0 AND NOT EXISTS ( SELECT * FROM page WHERE page_title=lt_title AND page_namespace=0) GROUP BY lt_title HAVING cnt >= $minreq" ;
my $sth = $dbh->prepare ( $sql ) ;
$sth->execute() ;
while ( my $ref = $sth->fetchrow_hashref() ) {
	print OUT $ref->{cnt} . "\t" . $ref->{lt_title} . "\n" ;
}
close OUT ;

# Check articles
my $fh ;
open $fh , ">$outfile.tmp" ;
#my $server = $wiki ;
#$server =~ s/_p$/-p/ ;
#$dbh = DBI->connect( "DBI:mysql:$wiki;host=$server.rrdb.toolserver.org",'magnus',$db_password) || die "Database connection not made: $DBI::errstr";

my $ig = $ignore{$wiki} || [] ;

#print join ( "\n" , @{$ig} ) . "\n" ;

open FILE , "$outfile.tmp1" ;
while ( <FILE> ) {
	next unless $_ =~ m/^\s*\d+\s(\S+)$/ ;
	my $article = $dbh->quote ( $1 ) ;

	my $ok = 1 ;
	foreach my $p ( @{$ig} ) {
		next unless $article =~ $p ;
		$ok = 0 ;
#		print "Ignored $article - matches $p\n" ;
		last ;
	}
	next unless $ok ;

	push @articles , $article ;
	check_articles ( $fh ) if $query_group <= scalar @articles ;
}
check_articles ( $fh ) ;
close FILE ;
close $fh ;

`rm -f $outfile.tmp1` ;
`sort -nr $outfile.tmp > $outfile.tmp2` ;
`rm -f $outfile.tmp` ;
`mv $outfile.tmp2 $outfile` ;



sub check_articles {
	my ( $fh ) = @_ ;
	return if 0 == scalar @articles ;
	my $al = join ( ',' , @articles ) ;
	@articles = () ;
	
	my $sql = "SELECT /* SLOW_OK */ count(*) AS cnt,lt_title FROM pagelinks,page,linktarget WHERE pl_target_id=lt_id AND lt_title IN ( $al ) AND lt_namespace=0 AND page_id=pl_from AND page_namespace=0 GROUP BY lt_title" ;
	my $sth = $dbh->prepare ( $sql ) ;
	$sth->execute() ;
	while ( my $ref = $sth->fetchrow_hashref() ) {
		next unless $ref->{'cnt'} >= $minreq ;
		print $fh $ref->{'cnt'} . "\t" . $ref->{'lt_title'} . "\n" ;
	}
}

